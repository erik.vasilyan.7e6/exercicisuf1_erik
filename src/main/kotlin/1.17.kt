import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 21/09/2022
* TITLE: És edat legal
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Introdueix tu edat: ")
    var edat = scanner.nextInt()

    println(edat >= 18)
}