import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 05/10/2022
* TITLE: Canvi mínim
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val euros = scanner.nextInt()
    val centims = scanner.nextInt()

    if (centims > 99)  println("$centims no és un nombre valid de centims")
    else {
        val eurosBillets500: Int = euros / 500
        val eurosBillets200: Int = (euros % 500) / 200
        val eurosBillets100: Int = ((euros % 500) % 200) / 100
        val eurosBillets50: Int = (((euros % 500) % 200) % 100) / 50
        val eurosBillets20: Int = ((((euros % 500) % 200) % 100) % 50) / 20
        val eurosBillets10: Int = (((((euros % 500) % 200) % 100) % 50) % 20) / 10
        val eurosBillets5: Int = ((((((euros % 500) % 200) % 100) % 50) % 20) % 10) / 5

        val eurosMonedes2: Int = (((((((euros % 500) % 200) % 100) % 50) % 20) % 10) % 5) / 2
        val eurosMonedes1: Int = (((((((euros % 500) % 200) % 100) % 50) % 20) % 10) % 5) % 2

        val centims50: Int = centims / 50
        val centims20: Int = (centims % 50) / 20
        val centims10: Int = ((centims % 50) % 20) / 10
        val centims5: Int = (((centims % 50) % 20) % 10) / 5
        val centims2: Int = ((((centims % 50) % 20) % 10) % 5) / 2
        val centims1: Int = ((((centims % 50) % 20) % 10) % 5) % 2

        println("Billets de 500 euros: $eurosBillets500")
        println("Billets de 200 euros: $eurosBillets200")
        println("Billets de 100 euros: $eurosBillets100")
        println("Billets de 50 euros: $eurosBillets50")
        println("Billets de 20 euros: $eurosBillets20")
        println("Billets de 10 euros: $eurosBillets10")
        println("Billets de 5 euros: $eurosBillets5")

        println("Monedes de 2 euros: $eurosMonedes2")
        println("Monedes de 1 euros: $eurosMonedes1")

        println("Monedes de 50 cèntims: $centims50")
        println("Monedes de 20 cèntims: $centims20")
        println("Monedes de 10 cèntims: $centims10")
        println("Monedes de 5 cèntims: $centims5")
        println("Monedes de 2 cèntims: $centims2")
        println("Monedes de 1 cèntims: $centims1")
    }
}