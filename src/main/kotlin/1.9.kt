import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 19/09/2022
* TITLE: Calcula el descompte
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Type the original prise: ")
    val originalPrise = scanner.nextDouble()
    print("Type the current prise: ")
    val currentPrise = scanner.nextDouble()

    val totalDiscount = (currentPrise * 100) / originalPrise
    val discount = 100 - totalDiscount

    print("Discount: $discount")
}