import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 05/10/2022
* TITLE: Any de traspàs
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val any = scanner.nextInt()

    if (any % 4 == 0 && (any % 100 != 0 || any % 400 == 0)) println("Si és de traspàs")
    else println("No és de traspàs")
}