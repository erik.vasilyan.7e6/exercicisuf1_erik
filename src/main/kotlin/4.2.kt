import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 14/10/2022
* TITLE: Calcula la mitjana
*/

fun main(args: Array<String>) {

    var resultat = 0.0
    var valor = 0.0

    for (arg in args) {
        val nombre = arg.toInt()
        resultat = resultat + nombre
        valor++
    }
    println(resultat / valor)
}