import java.util.Scanner

/*
* AUTHOR: Erik Vasilyan
* DATE: 21/10/2022
* TITLE: Quants sumen...?
*/

fun main(args: Array<String>) {
    val scanner = Scanner(System.`in`)

    val n = scanner.nextInt()

    for(i in args.indices) {

        for (j in args.indices) {
            if (args[i].replace(",","").toInt() + args[j].replace(",","").toInt() == n) {
                println("${args[i].replace(",","")} ${args[j].replace(",","")}")
            }
        }
    }
}