import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 29/09/2022
* TITLE: En rang
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val number1 = scanner.nextInt()
    val number2 = scanner.nextInt()
    val number3 = scanner.nextInt()
    val number4 = scanner.nextInt()
    val number5 = scanner.nextInt()

    println(number5 in number1..number2 && number5 in number3..number4)
}