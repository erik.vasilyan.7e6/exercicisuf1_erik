import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 05/10/2022
* TITLE: Quants dies té el mes
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val mes = scanner.nextInt()

    when (mes) {
        1 -> println("31")
        2 -> println("28")
        3 -> println("31")
        5 -> println("31")
        7 -> println("31")
        8 -> println("31")
        10 -> println("31")
        12 -> println("31")
        else -> println("30")
    }
}