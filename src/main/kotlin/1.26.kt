import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 21/09/2022
* TITLE: Fes-me minúscula
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Introdueix un caràcter: ")
    var char = scanner.next().single()

    println(char.lowercaseChar())
}