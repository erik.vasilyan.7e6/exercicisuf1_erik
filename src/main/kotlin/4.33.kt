import java.util.Scanner

/*
* AUTHOR: Erik Vasilyan
* DATE: 17/11/2022
* TITLE: Comptant freqüències
*/

fun main() {
    val scanner = Scanner(System.`in`)
    val sequnece = mutableListOf<Int>()
    val finalSequence = mutableListOf<Int>()
    val n = scanner.nextInt()

    repeat(n) { sequnece.add(scanner.nextInt()) }
    sequnece.sort()

    for (element in sequnece) {
        var repeats = 0
        for (otherElem in sequnece) {
            if (element == otherElem) repeats++
        }
        if (element !in finalSequence) {
            finalSequence.add(element)
            finalSequence.add(repeats)
        }
    }

    for (i in 0 until finalSequence.size step 2) println("${finalSequence[i]}: ${finalSequence[i+1]}")
}
