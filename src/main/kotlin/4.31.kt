import java.util.Scanner

/*
* AUTHOR: Erik Vasilyan
* DATE: 17/11/2022
* TITLE: Comptant a’s
*/

fun main() {
    val scanner = Scanner(System.`in`)
    val sequence = mutableListOf<String>()
    var n = 0

    sequence.add(scanner.nextLine())

    val frase = sequence[0]

    for (letra in frase) {
        if (letra == 'a') n++
        else if (letra == '.') break
    }
    println(n)
}