import java.util.Scanner

/*
* AUTHOR: Erik Vasilyan
* DATE: 29/11/2022
* TITLE: És la solució d'un Sudoku?
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val n = scanner.nextInt()
    val totsMatrius = mutableListOf<List<List<Int>>>()
    var correct = true

    repeat(n) {
        val matriu = mutableListOf<List<Int>>()
        for (i in 1..9) {
            val line = mutableListOf<Int>()
            for (j in 1..9) {
                line.add(scanner.nextInt())
            }
            matriu.add(line)
        }
        totsMatrius.add(matriu)
        println("")
    }

    for (k in totsMatrius.indices) {

        // fila
        for (m in totsMatrius[k].indices) {
            // println(totsMatrius[k][m])
            val list = mutableListOf<Int>()
            for (number in totsMatrius[k][m]) {
                if (number !in list) list.add(number)
            }
            if (list.size != 9) {
                correct = false
                break
            }
        }
        // columna
        for (l in totsMatrius[k].indices) {
            val list = mutableListOf<Int>()
            for (o in totsMatrius[k][l].indices) {
                if (totsMatrius[k][o][l] !in list) list.add(totsMatrius[k][o][l])
            }
            if (list.size != 9) {
                correct = false
                break
            }
        }
        if (correct) println("si")
        else println("no")
    }
}