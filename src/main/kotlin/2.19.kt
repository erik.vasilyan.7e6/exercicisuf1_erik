import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 05/10/2022
* TITLE: Quina nota he tret?
*/

fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)

    val nota = scanner.nextDouble()

    if (nota >= 0 && nota < 5) println("Insuficient")
    else if (nota >= 5 && nota < 6) println("Suficient")
    else if (nota >= 6 && nota < 7) println("Bé")
    else if (nota >= 7 && nota < 9) println("Notable")
    else if (nota >= 9 && nota < 10) println("Excel·lent")
    else println("Nota invàlida")
}