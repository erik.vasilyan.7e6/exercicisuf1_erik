import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 06/10/2022
* TITLE: Pinta X números
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val number = scanner.nextInt()

    for (i in 1..number) println(i) // for (int i=0; i<number; i++)
}