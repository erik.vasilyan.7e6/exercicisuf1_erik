import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 21/09/2022
* TITLE: Qui riu últim riu millor
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Introdueix el primer nombre: ")
    var primerNombre = scanner.nextInt()
    print("Introdueix el segon nombre: ")
    var segonNombre = scanner.nextInt()
    print("Introdueix el terçer nombre: ")
    var tercerNombre = scanner.nextInt()
    print("Introdueix el quart nombre: ")
    var quartNombre = scanner.nextInt()
    print("Introdueix el cinque nombre: ")
    var cinqueNombre = scanner.nextInt()

    println(cinqueNombre > primerNombre && cinqueNombre > segonNombre && cinqueNombre > tercerNombre && cinqueNombre > quartNombre)
}