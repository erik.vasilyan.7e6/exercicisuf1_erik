import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 21/09/2022
* TITLE: Programa adolescent
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Introdueix el valor booleà: ")
    var boolean = scanner.nextBoolean()

    println(!boolean)
}