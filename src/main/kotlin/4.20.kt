import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 21/10/2022
* TITLE: Distància d'Hamming
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val firstADN = scanner.next()
    val secondADN = scanner.next()
    var n = 0

    if (firstADN.length == secondADN.length) {
        for (i in firstADN.indices) {
            if (firstADN[i] != secondADN[i]) n++
        }
        println(n)
    }
    else println("Entrada no valida")
}