import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 06/10/2022
* TITLE: Eleva’l
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val numero = scanner.nextLong() // El numero
    val elevado = scanner.nextLong() // Elevado

    var numeroElevado: Long = numero
    for (i in 1 until elevado) { // i=1; i < elevado; i++
        numeroElevado = numeroElevado * numero
        /*
            El numero elevado al principio es igual al numero, es decir el primer resultado que obtiene numeroElevado
            es numero*numero y lo guarda en numeroElevado, despues vuelve a executarlo y el numeroElevado ya es igual
            numero*numero*numero porque numero*numero es el anterior numeroElevado
         */
    }
    println(numeroElevado)
}