import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 22/09/2022
* TITLE: Quant de temps?
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Introdueix el nombre de segons: ")
    var segons = scanner.nextInt()

    val hora: Int = (segons / 3600)
    val minuts: Int = (segons / 60) % 60
    segons %= 60

    println("$hora hora $minuts minuts $segons segons")
}