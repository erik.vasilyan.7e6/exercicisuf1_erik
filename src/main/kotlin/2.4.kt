import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 29/09/2022
* TITLE: Té edat per treballar
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val number = scanner.nextInt()

    if (number >= 16 && number <= 65) println("Té edat per treballar")
    else println("No té edat per treballar")
}