import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 05/10/2022
* TITLE: Comprova la data
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val dia = scanner.nextInt()
    val mes = scanner.nextInt()
    val any = scanner.nextInt()

    if ((dia !in 1..31) || mes !in 1..12 || any !in 0..2022) println("La data no és correcta")
    else {
        when(mes) {
            1, 3, 5, 7, 8, 10, 12 -> println("La data és correcta")
            2 -> if ((any % 4 == 0 && (any % 100 != 0 || any % 400 == 0)) && dia !in 1..29) println("La data no és correcta")
            else if (!(any % 4 == 0 && (any % 100 != 0 || any % 400 == 0)) && dia > 28) println("La data no és correcta")
            else println("La data és correcta")
            4,6,9,11 -> if (dia > 30) println("La data no és correcta")
        }
    }
}