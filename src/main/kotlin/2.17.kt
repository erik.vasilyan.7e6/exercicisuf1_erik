import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 05/10/2022
* TITLE: Puges o baixes?
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val num1 = scanner.nextInt()
    val num2 = scanner.nextInt()
    val num3 = scanner.nextInt()

    if (num1 < num2 && num2 < num3) println("Ascendent")
    else if (num1 > num2 && num2 > num3) println("Descendent")
    else println("Cap de les dues")
}