import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 29/09/2022
* TITLE: És un bitllet vàlid
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val number = scanner.nextInt()

    println(number == 5 || number == 10 || number == 50 || number == 100 || number == 200 || number == 500)
}