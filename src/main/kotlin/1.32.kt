import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 22/09/2022
* TITLE: Calcula el capital (no LA capital)
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Introdueix el capital: ")
    var capital = scanner.nextInt()
    print("Introdueix els anys: ")
    var anys = scanner.nextInt()
    print("Introdueix els interessos: ")
    var interessos = scanner.nextInt()

    var capitalFinal = capital + ((capital * interessos/100) * anys)

    println(capitalFinal)
}