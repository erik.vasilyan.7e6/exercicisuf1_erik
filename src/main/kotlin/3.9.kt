import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 07/10/2022
* TITLE: És primer?
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val nombre = scanner.nextInt()
    var primer = true

    if (nombre == 0 || nombre == 1) println("No és primer")
    else {
        for (i in 2..nombre/2) {
            if (nombre % i == 0) {
                println("No és primer")
                primer = false
                break
            }
        }
    }
    if(primer) println("És primer")
}