import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 13/10/2022
* TITLE: Fibonacci
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val n = scanner.nextInt()
    var a = 0
    var b = 1
    var c: Int

    print("$a $b")

    for (i in 2 until n) {
        c = a + b
        a = b
        b = c
        print(" $c")
    }
}