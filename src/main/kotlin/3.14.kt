import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 13/10/2022
* TITLE: Endevina el número
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val random = (1..100).random()

    do {
        val number = scanner.nextInt()
        if (number > random) {
            println("Massa alt")
        }
        else if (number < random) {
            println("Massa baix")
        }
        else {
            println("Has encertat!")
        }
    } while (number != random)
}