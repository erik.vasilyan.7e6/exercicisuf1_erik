/*
* AUTHOR: Erik Vasilyan
* DATE: 15/10/2022
* TITLE: Mínim i màxim
*/

fun main(args: Array<String>) {

    var maxNum = args[0].toInt()
    var minNum = args[0].toInt()

    for (i in args.indices) {
        if(args[i].toInt() > maxNum) maxNum = args[i].toInt()
        else if(args[i].toInt() < minNum) minNum = args[i].toInt()
    }
    println("$minNum $maxNum")
}

