import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 21/09/2022
* TITLE: Fes-me majúscula
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Introdueix un caràcter: ")
    val char = scanner.next().single()

    println(char.uppercaseChar())
}