import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 21/10/2022
* TITLE: Són iguals? (2)
*/

fun main() {
    val scanner = Scanner(System.`in`)

    var userString = scanner.next()

    do {
        val userChar = scanner.next().single()

        for(char in userString) {
            if (char == userChar) userString = userString.replace(char.toString(), "")
        }

    } while (userChar != '0')
    println(userString)
}