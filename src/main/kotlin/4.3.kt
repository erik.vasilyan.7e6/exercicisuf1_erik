import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 14/10/2022
* TITLE: Calcula la lletra del dni
*/

fun main (array: Array<String>) {
    val scanner = Scanner(System.`in`)

    val DNI = scanner.nextInt()

    var letras = arrayOf("T", "R", "W", "A", "G", "M", "Y", "F", "P", "D", "X", "B", "N", "J", "Z", "S", "Q", "V", "H", "L", "C", "K", "E")

    println("${DNI}${letras[DNI%23]}")
}