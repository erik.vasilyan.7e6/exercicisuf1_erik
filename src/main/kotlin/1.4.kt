import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 19/09/2022
* TITLE: Calcula l’àrea
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Type the width: ")
    val width = scanner.nextInt()
    print("Type the height: ")
    val height = scanner.nextInt()

    val area = width * height
    print("Area: $area")
}