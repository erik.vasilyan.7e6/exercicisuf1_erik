import java.util.Scanner

/*
* AUTHOR: Erik Vasilyan
* DATE: 24/11/2022
* TITLE: Suma de matrius
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val n = scanner.nextInt()
    val totsMatrius = mutableListOf<List<List<Int>>>()

    repeat(2) {
        val matriu = mutableListOf<List<Int>>()
        for (i in 1..n) {
            val line = mutableListOf<Int>()
            for (j in 1..n) {
                line.add(scanner.nextInt())
            }
            matriu.add(line)
        }
        totsMatrius.add(matriu)
        println("")
    }

    for (k in 0 until n) {
        for (m in 0 until n) {
            val suma = totsMatrius[0][k][m] + totsMatrius[1][k][m]
            print("$suma ")
        }
        println("")
    }
}