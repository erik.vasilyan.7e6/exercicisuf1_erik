import java.lang.Math.sqrt
import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 22/09/2022
* TITLE: Equacions de segon grau
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Introdueix el a: ")
    val a = scanner.nextInt()
    print("Introdueix el b: ")
    val b = scanner.nextInt()
    print("Introdueix el c: ")
    val c = scanner.nextInt()

    val x1 = (-b - Math.sqrt(((b * b) - 4 * a * c).toDouble())) / (2 * a)
    val x2 = (-b + Math.sqrt(((b * b) - 4 * a * c).toDouble())) / (2 * a)

    println("$x1\n$x2")
}