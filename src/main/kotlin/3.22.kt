import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 15/10/2022
* TITLE: Rombe d’*
*/

fun main() {

    val scanner = Scanner(System.`in`)

    val rows = scanner.nextInt()

    for (i in 1 until rows) {
        for(k in i until rows)
        {
            print(" ")
        }

        for (j in 1..2*i-1) {
            print("*")
        }
        println()
    }

    for (i in rows downTo 1) {
        for(k in i until rows)
        {
            print(" ")
        }

        for (j in 1..2*i-1) {
            print("*")
        }
        println()
    }
}