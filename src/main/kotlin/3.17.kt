import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 13/10/2022
* TITLE: Factorial!
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val n = scanner.nextInt()
    var resultat = 1

    for (i in 1..n) {
        resultat = resultat * i
    }
    println(resultat)
}