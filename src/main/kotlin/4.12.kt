import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 19/10/2022
* TITLE: Ordena l’array (up)
*/

fun main(args: Array<String>) {

    var temp: String

    for (i in args.indices) {
        for (j in i+1 until args.size) {
            if(args[i].toInt() > args[j].toInt()) {
                temp = args[i]
                args[i] = args[j]
                args[j] = temp
            }
        }
        print("${args[i]} ")
    }
}