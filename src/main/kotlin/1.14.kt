import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 21/09/2022
* TITLE: Divisor de compte
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Introdueix el número de començals: ")
    val comencals = scanner.nextInt()
    print("Introdueix el preu d'un sopar: ")
    val preu = scanner.nextDouble()

    val cost = preu / comencals

    println("Cost: $cost\u20AC")
}