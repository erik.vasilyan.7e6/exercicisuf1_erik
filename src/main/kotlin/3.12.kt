import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 13/10/2022
* TITLE: Divisible per 3 i per 5
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val n = scanner.nextInt()

    for (i in 1..n) {

        if (i % 3 == 0 && i % 5 == 0) {
            println("$i divisible per 3 i per 5")
        }
        else if (i % 3 == 0) {
            println("$i divisible per 3")
        }
        else if (i % 5 == 0) {
            println("$i divisble per 5")
        }
    }
}