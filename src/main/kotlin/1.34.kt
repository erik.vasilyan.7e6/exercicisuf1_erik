import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 22/09/2022
* TITLE: Hola Usuari
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Introdueix el teu nom: ")
    var nom = scanner.next()

    println("Bon dia $nom")
}