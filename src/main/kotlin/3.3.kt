import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 06/10/2022
* TITLE: Imprimeix el rang
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val number1 = scanner.nextInt()
    val number2 = scanner.nextInt()

    if (number2 > number1) {
        for (i in number1 until number2) {
            print("${i},")
        }
        print(number2)
    }
    else println(number1)
}