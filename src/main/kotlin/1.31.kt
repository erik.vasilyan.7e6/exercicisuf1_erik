import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 22/09/2022
* TITLE: De metre a peun
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Introdueix el nombre de metres: ")
    val metres = scanner.nextInt()

    val peus: Double = metres * 39.37 / 12

    println(peus)
}