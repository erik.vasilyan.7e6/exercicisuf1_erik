import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 15/10/2022
* TITLE: Triangle de nombres
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val rows = scanner.nextInt()

    for(i in 1..rows) {
        for(j in 1..i) {
            print(j)
        }
        println("")
    }
}