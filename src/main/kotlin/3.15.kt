import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 13/10/2022
* TITLE: Endevina el número (ara amb intents)
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val random = (1..100).random()
    var intents = 0
    println(random)

    do {
        val number = scanner.nextInt()
        if (number > random) {
            println("Massa alt")
            intents++
        }
        else if (number < random) {
            println("Massa baix")
            intents++
        }
        else {
            println("Has encertat!")
            break
        }
        if (intents == 6) println("Has perdut :(")
    } while (intents < 6)
}