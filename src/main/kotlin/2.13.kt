import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 05/10/2022
* TITLE: Quin dia de la setmana?
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val dia = scanner.nextInt()
    val mes = scanner.nextInt()
    val any = scanner.nextInt()

    if ((dia < 1 || dia > 31) || (mes < 1 || mes > 12) || (any < 0 || any > 2022)) println("La data no és correcta")
    else {
        val yearLastTwoDigits = any % 100
        val yearCode = (yearLastTwoDigits + (yearLastTwoDigits / 4)) % 7

        var monthCode = 0
        when (mes) {
            1 -> monthCode = 0
            2 -> monthCode = 3
            3 -> monthCode = 3
            4 -> monthCode = 6
            5 -> monthCode = 1
            6 -> monthCode = 4
            7 -> monthCode = 6
            8 -> monthCode = 2
            9 -> monthCode = 5
            10 -> monthCode = 0
            11 -> monthCode = 3
            12 -> monthCode = 5
        }

        val centuryCode: Int = when (any) {
            in 1700..1799 -> 4
            in 1800..1899 -> 2
            in 1900..1999 -> 0
            else -> 6
        }

        val dayOfWeek:Int = if (mes == 1 || mes == 2) ((yearCode + monthCode + centuryCode + dia) % 7) - 1
        else (yearCode + monthCode + centuryCode + dia) % 7

        when (dayOfWeek) {
            0 -> println("Diumenge")
            1 -> println("Dilluns")
            2 -> println("Dimarts")
            3 -> println("Dimecres")
            4 -> println("Dijous")
            5 -> println("Divendres")
            6 -> println("Dissabte")
        }
    }
}