import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 29/09/2022
* TITLE: Afegeix un segon (2)
*/

fun main() {
    val scanner = Scanner(System.`in`)

    var hora = scanner.nextInt()
    var minuts = scanner.nextInt()
    var segons = scanner.nextInt()


    if (++segons >= 60) {
        segons %= 60
        minuts++
    }
    if (minuts >= 60) {
        minuts %= 60
        hora++
    }

    hora %= 24

    val horaFormat = hora.toString().padStart(2, '0')
    val minutsFormat = minuts.toString().padStart(2, '0')
    val segonsFormat = segons.toString().padStart(2, '0')

    println("$horaFormat:$minutsFormat:$segonsFormat")
}