import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 05/10/2022
* TITLE: Calculadora
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val firstNumber = scanner.nextInt()
    val secondNumber = scanner.nextInt()
    val operator = scanner.next().single()

    when (operator) {
        '+' -> println(firstNumber + secondNumber)
        '-' -> println(firstNumber - secondNumber)
        '*' -> println(firstNumber * secondNumber)
        '/' -> println(firstNumber / secondNumber)
        '%' -> println(firstNumber % secondNumber)
    }
}