import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 29/09/2022
* TITLE: Màxim de 3 nombres enters
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val number1 = scanner.nextInt()
    val number2 = scanner.nextInt()
    val number3 = scanner.nextInt()

    if (number1 > number2 && number1 > number3) println(number1)
    else if (number2 > number1 && number2 > number3) println(number2)
    else if (number3 > number1 && number3 > number2) println(number3)
}