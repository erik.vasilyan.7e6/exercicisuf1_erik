import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 21/09/2022
* TITLE: Nombres decimals iguals
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Introdueix el primer nombre: ")
    var primerNombre = scanner.nextDouble()

    print("Introdueix el segon nombre: ")
    var segonNombre = scanner.nextDouble()

    println(primerNombre == segonNombre)
}