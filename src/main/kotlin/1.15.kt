import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 21/09/2022
* TITLE: Afegeix un segon
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Introdueix els segons: ")
    var segons = scanner.nextInt()

    println((segons+1) % 60)
}