import java.time.LocalDate

/*
* AUTHOR: Erik Vasilyan
* DATE: 22/09/2022
* TITLE: Ajuda per la màquina de viatge en el temps
*/

fun main() {

    val dataAvui = LocalDate.now()

    println("Avui es $dataAvui")
}