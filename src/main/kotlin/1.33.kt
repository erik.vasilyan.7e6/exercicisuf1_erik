import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 22/09/2022
* TITLE: És divisible
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Introdueix el primer nombre: ")
    var primerNombre = scanner.nextInt()
    print("Introdueix el segon nombre: ")
    var segonNombre = scanner.nextInt()

    println((segonNombre%primerNombre) == 0)
}