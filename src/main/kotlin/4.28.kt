import java.util.Scanner
import kotlin.math.pow
import kotlin.math.sqrt

/*
* AUTHOR: Erik Vasilyan
* DATE: 17/11/2022
* TITLE: Elimina les repeticions
*/

fun main() {
    val scanner = Scanner(System.`in`)

    var numero = scanner.nextInt()

    var nOfNumbers = numero.toString().length

    println("nOfNumbers: $nOfNumbers")

    while (numero > 0) {
        val a = nOfNumbers * 10
        println("a: $a")
        numero -= a
        nOfNumbers--
        println("numero: $numero")
    }
}