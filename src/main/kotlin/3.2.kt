import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 06/10/2022
* TITLE: Calcula la suma dels N primers
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val number = scanner.nextInt()

    var resultat = 0
    for (i in 1..number) {
        resultat = resultat + i
    }
    println(resultat)
}