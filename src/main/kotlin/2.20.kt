import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 05/10/2022
* TITLE: Conversor d’unitats
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val pes = scanner.nextDouble()
    val unitat = scanner.next()

    if (unitat == "G") {
        val pesTN = pes / 1000000
        val pesKG = pes / 1000
        println("$pesTN TN")
        println("$pesKG KG")
    }
    else if (unitat == "KG") {
        val pesTN = pes / 1000
        val pesG = pes * 1000
        println("${pesTN.toInt()} TN")
        println("${pesG.toInt()} G")
    }
    else if (unitat == "TN") {
        val pesKG = pes * 1000
        val pesG = pes * 1000000
        println("$pesKG KG")
        println("$pesG G")
    }
    else println("ERROR")
}