import java.util.Scanner

/*
* AUTHOR: Erik Vasilyan
* DATE: 24/11/2022
* TITLE: Segona paraula més gran
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val n = scanner.nextInt()

    val paraules = mutableListOf<String>()
    val finalParaules = mutableListOf<String>()

    repeat(n) { paraules.add(scanner.next()) }

    for (element in paraules) {
        if (element !in finalParaules) finalParaules.add(element)
    }

    finalParaules.sort()
    println(finalParaules[1])
}