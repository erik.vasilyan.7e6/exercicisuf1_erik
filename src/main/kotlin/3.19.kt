import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 13/10/2022
* TITLE: Multiplicador de primers
*/

fun main() {
    val scanner = Scanner(System.`in`)
    var resultado: Long = 1

    val n = scanner.nextInt()

    for (i in 1..n) {
        var primer = true
        if (i >= 2) {
            for (j in 2..i / 2) {
                if (i % j == 0) {
                    // No es primer
                    primer = false
                    break
                }
            }
        }

        if(primer) {
            // És primer
            resultado *= i
        }
    }
    println(resultado)
}
