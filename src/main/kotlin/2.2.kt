import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 29/09/2022
* TITLE: Salari
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val hores = scanner.nextInt()

    if (hores <= 40) println(hores * 40)
    else println(hores * 40 + ((hores-40) * 40/2))
}