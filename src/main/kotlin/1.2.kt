import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 19/09/2022
* TITLE: Dobla l'enter
*/

fun main() {
    val scanner = Scanner(System.`in`)
    print("Type a number: ")
    val userInputValue = scanner.nextInt()
    val result = userInputValue * 2
    println("Result: $result")
}