import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 21/10/2022
* TITLE: Palíndrom
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val userString = scanner.next()
    var correct = true

    for(i in userString.indices) {
        if (userString[i] == userString[userString.length-1-i]) correct = true
        else {
            correct = false
            break
        }
    }
    if (correct) println("Es un palindrom")
    else println("No es un palindrom")
}