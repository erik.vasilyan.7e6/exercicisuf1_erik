import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 22/09/2022
* TITLE: Equacions de segon grau
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Introdueix el primer nombre: ")
    var primerNombre = scanner.nextInt()
    print("Introdueix el segon nombre: ")
    var segonNombre = scanner.nextInt()
    print("Introdueix el terçer nombre: ")
    var tercerNombre = scanner.nextInt()
    print("Introdueix el quart nombre: ")
    var quartNombre = scanner.nextInt()

    println(primerNombre == 10 || segonNombre == 10 || tercerNombre == 10 || quartNombre == 10)
}