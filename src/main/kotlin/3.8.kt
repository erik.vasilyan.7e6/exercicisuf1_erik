import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 07/10/2022
* TITLE: Nombre de dígits
*/

fun main() {
    val scanner = Scanner(System.`in`)

    var number = scanner.nextInt()
    var digits = 0
    val numberSave = number

    if (number >= 10) {
        while (number != 0) {
            number /= 10
            digits++
        }
        println("El número $numberSave té $digits digits.")
    }
    else println("El número $numberSave té 1 digit.")
}