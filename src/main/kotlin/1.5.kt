import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 19/09/2022
* TITLE: Operació boja
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Type the first number: ")
    val firstNumber = scanner.nextInt()
    print("Type the second number: ")
    val secondNumber = scanner.nextInt()
    print("Type the third number: ")
    val thirdNumber = scanner.nextInt()
    print("Type the fourth number: ")
    val fourthNumber = scanner.nextInt()

    val result = (firstNumber + secondNumber) * (thirdNumber % fourthNumber)

    print("Result: $result")
}