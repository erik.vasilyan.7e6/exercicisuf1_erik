import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 13/10/2022
* TITLE: Coordenades en moviment
*/

fun main() {
    val scanner = Scanner(System.`in`)

    var primeraPosicio = 0
    var segonaPosicio = 0

    do {
        val char = scanner.next().single()

        when (char) {
            'n' -> segonaPosicio--
            's' -> segonaPosicio++
            'o' -> primeraPosicio--
            'e' -> primeraPosicio++
        }

    } while (char != 'z')
    println("($primeraPosicio, $segonaPosicio)")
}