import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 21/10/2022
* TITLE: Parèntesis
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val parentesis = scanner.next()
    var correct = true
    var counter = 0

    for(par in parentesis) {
        if (par == '(') counter++
        else counter--
        if (counter < 0) break

    }

    println("--------")
    if (counter != 0) println("no")
    else println("si")
}