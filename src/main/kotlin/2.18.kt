import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 05/10/2022
* TITLE: Valor absolut
*/

fun main() {
    val scanner = Scanner(System.`in`)

    var number = scanner.nextInt()

    if (number < 0) number = (number * number) / -number

    println(number)

    // println(Math.abs(number))
}