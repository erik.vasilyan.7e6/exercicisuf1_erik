import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 05/10/2022
* TITLE: Calcula la lletra del dni
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val numeroDNI = scanner.nextInt()

        when (numeroDNI % 23) {
            0 -> println("${numeroDNI}T")
            1 -> println("${numeroDNI}R")
            2 -> println("${numeroDNI}W")
            3 -> println("${numeroDNI}A")
            4 -> println("${numeroDNI}G")
            5 -> println("${numeroDNI}M")
            6 -> println("$${numeroDNI}Y")
            7 -> println("${numeroDNI}F")
            8 -> println("${numeroDNI}P")
            9 -> println("${numeroDNI}D")
            10 -> println("${numeroDNI}X")
            11 -> println("${numeroDNI}B")
            12 -> println("${numeroDNI}N")
            13 -> println("${numeroDNI}J")
            14 -> println("${numeroDNI}Z")
            15 -> println("${numeroDNI}S")
            16 -> println("${numeroDNI}Q")
            17 -> println("${numeroDNI}V")
            18 -> println("${numeroDNI}H")
            19 -> println("${numeroDNI}L")
            20 -> println("${numeroDNI}C")
            21 -> println("${numeroDNI}K")
            22 -> println("${numeroDNI}E")
        }
}