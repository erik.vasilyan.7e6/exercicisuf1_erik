import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 07/10/2022
* TITLE: Revers de l’enter
*/

fun main() {
    val scanner = Scanner(System.`in`)

    var number = scanner.nextInt()
    var revNumber = 0

    while (number > 0) {
        revNumber = revNumber * 10 + number % 10
        number /= 10
    }
    println(revNumber)
}