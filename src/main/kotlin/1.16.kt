import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 21/09/2022
* TITLE: Transforma l’enter
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Introdueix un nombre enter: ")
    var number1 = scanner.nextInt()

    println(number1.toDouble())
}