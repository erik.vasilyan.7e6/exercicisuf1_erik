import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 06/10/2022
* TITLE: Taula de multiplicar
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val number = scanner.nextInt()

    for (i in 1..10) {
        val resultat = number * i
        println("${number}x$i = $resultat")
    }
}