import java.util.Scanner
import kotlin.math.abs

/*
* AUTHOR: Erik Vasilyan
* DATE: 29/11/2022
* TITLE: Quadrats màgics
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val repeat = true
    var correct = true

    while (repeat) {
        val n = scanner.nextInt()
        val listOfSumm = mutableListOf<Int>()

        val matriu = mutableListOf<List<Int>>()
        for (i in 1..n) {
            val line = mutableListOf<Int>()
            for (j in 1..n) {
                line.add(scanner.nextInt())
            }
            matriu.add(line)
        }

        // files
        for (k in matriu.indices) {
            listOfSumm.add(matriu[k].sum())
        }

        // columnes
        for (l in matriu.indices) {
            val columnesSum = mutableListOf<Int>()
            for (o in matriu[l].indices) {
                columnesSum.add(matriu[o][l])
            }
            listOfSumm.add(columnesSum.sum())
        }

        // diagonal 1
        var diagonalSum = mutableListOf<Int>()
        for (u in matriu.indices) {
            diagonalSum.add(matriu[u][u])
        }
        listOfSumm.add(diagonalSum.sum())

        // diagonal 2
        diagonalSum = mutableListOf()
        for (p in matriu.size-1 downTo 0) {
            diagonalSum.add(matriu[p][abs(p-matriu.lastIndex)])
        }
        listOfSumm.add(diagonalSum.sum())

        // comprobación
        for (element in listOfSumm) {
            if (listOfSumm[0] == element) correct = true
            else {
                correct = false
                break
            }
        }
        if (correct) println("sí")
        else println("no")
    }
}
