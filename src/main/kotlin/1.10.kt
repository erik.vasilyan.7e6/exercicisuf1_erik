import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 19/09/2022
* TITLE: Quina és la mida de la meva pizza?
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Type the diameter of a round: ")
    val diameter = scanner.nextDouble() 

    val area = Math.PI * ((diameter / 2) * (diameter / 2))

    print("Area: ${String.format("%.2f", area)}")
}