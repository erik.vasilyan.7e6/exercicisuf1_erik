/*
* AUTHOR: Erik Vasilyan
* DATE: 19/09/2022
* TITLE: Hello World
*/

fun main() {
    println("Hello World!")
}