import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 19/09/2022
* TITLE: Pupitres
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Type the number of students in first class: ")
    val firstClassStudents = scanner.nextInt()
    print("Type the number of students in second class: ")
    val secondClassStudents = scanner.nextInt()
    print("Type the number of students in third class: ")
    val thirdClassStudents = scanner.nextInt()

    val rest = (firstClassStudents + secondClassStudents + thirdClassStudents) % 2
    val result = ((firstClassStudents + secondClassStudents + thirdClassStudents) / 2) + rest

    println("Result: $result")
}