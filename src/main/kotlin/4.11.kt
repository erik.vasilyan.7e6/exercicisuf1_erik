import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 19/10/2022
* TITLE: Quants parells i quants senars?
*/

fun main(args: Array<String>) {
    val scanner = Scanner(System.`in`)

    var primer = true
    var parells = 0
    var senars = 0

    for (arg in args) {
        if (arg.toInt() % 2 == 0) parells++
        else senars++
    }
    println("Parells: $parells")
    println("Senars: $senars")
}
