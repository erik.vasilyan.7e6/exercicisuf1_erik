import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 21/09/2022
* TITLE: Quina temperatura fa?
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Type the temperature: ")
    val temperature = scanner.nextDouble()
    print("Type the increase of the temperature: ")
    val increase = scanner.nextDouble()

    val actualTemperature = temperature + increase

    println("La temperatura actual és: $actualTemperature\u2103")
}