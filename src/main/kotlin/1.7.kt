import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 19/09/2022
* TITLE: Número següent
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Type a number: ")
    val firstNumber = scanner.nextInt()

    val result =  firstNumber + 1

    print("Després ve el: $result")
}