import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 22/09/2022
* TITLE: Creador de targetes de treball
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Introdueix el teu nom: ")
    var nom = scanner.next()
    print("Introdueix el teu cognom: ")
    var cognom = scanner.next()
    print("Introdueix el teu edat: ")
    var edat = scanner.nextInt()

    println("Empleada: $nom $cognom - Despatx: $edat")
}