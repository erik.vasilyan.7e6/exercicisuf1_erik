import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 05/10/2022
* TITLE: És vocal o consonant?
*/

fun main() {
    val scanner = Scanner(System.`in`)

    var char = scanner.next().single()

    char = char.lowercaseChar()

    when (char) {
        'a' -> println("Vocal")
        'e' -> println("Vocal")
        'i' -> println("Vocal")
        'o' -> println("Vocal")
        'u' -> println("Vocal")
        else -> println("Consonant")
    }
}