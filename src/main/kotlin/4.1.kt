import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 14/10/2022
* TITLE: Suma els valors
*/

fun main(args: Array<String>) {

    var resultat = 0

    for (arg in args) {
        val nombre = arg.toInt()
        resultat = resultat + nombre
    }
    println(resultat)
}