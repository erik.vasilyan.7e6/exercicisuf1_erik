import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 29/09/2022
* TITLE: Quina pizza és més gran?
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val diamterePizzaRodona = scanner.nextInt()
    val costat1PizzaRectangular = scanner.nextInt()
    val costat2PizzaRectangular = scanner.nextInt()

    val areaPizzaRodona = Math.PI * (diamterePizzaRodona/2) * (diamterePizzaRodona/2)
    val areaPizzaRectangular = costat1PizzaRectangular * costat2PizzaRectangular

    if (areaPizzaRodona > areaPizzaRectangular) println("Pizza rodona: $areaPizzaRodona")
    else if (areaPizzaRectangular > areaPizzaRodona) println("Pizza rectangular: $areaPizzaRectangular")
    else println("Son iguals de area")
}