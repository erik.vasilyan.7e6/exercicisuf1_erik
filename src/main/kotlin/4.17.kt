import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 21/10/2022
* TITLE: Són iguals? (2)
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val firstString = scanner.next()
    val secondString = scanner.next()
    var correct = true

    for (i in firstString.indices) {
        correct = firstString[i].lowercaseChar() == secondString[i].lowercaseChar()
    }
    if (correct) println("Son iguals")
    else println("No son iguals")
}