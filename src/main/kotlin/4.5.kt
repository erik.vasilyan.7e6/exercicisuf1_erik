import java.util.*
/*
* AUTHOR: Erik Vasilyan
* DATE: 14/10/2022
* TITLE: En quina posició?
*/

fun main (args: Array<String>) {
    val scanner = Scanner(System.`in`)

    val nombre = scanner.nextInt()
    val array = arrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)

    if (nombre in array) {
        println("${array.indexOf(nombre)}")
    }
    else println("No està contingut")
}