import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 13/10/2022
* TITLE: Suma de fraccions
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val n = scanner.nextInt()

    var resultat = 0.0

    for (i in 1..n) {
        resultat += 1.0 / i
    }
    println(resultat)
}