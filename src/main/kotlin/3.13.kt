import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 13/10/2022
* TITLE: Logaritme natural de 2
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val n = scanner.nextInt()
    var sign = 1.0
    var sum = 0.0

    for (i in 1..n) {
        sign *= -1
        sum -= (sign * 1.0 / i)
    }
    println(sum)
}