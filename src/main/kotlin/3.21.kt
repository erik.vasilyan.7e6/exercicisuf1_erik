import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 15/10/2022
* TITLE: Triangle invertit d'*
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val rows = scanner.nextInt()



    for(i in rows downTo 1) {
        for(k in i..(rows-1))
        {
            print(" ")
        }

        for(j in 1..i) {
            print("*")
        }
        println("")
    }
}