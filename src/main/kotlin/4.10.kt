/*
* AUTHOR: Erik Vasilyan
* DATE: 19/10/2022
* TITLE: Busca el que falta
*/

fun main(args: Array<String>) {

    for(i in args.indices) {
        if (args[i].toInt()+1 != args[i+1].toInt()) {
            println(args[i].toInt()+1)
            break
        }
    }
}