import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 21/09/2022
* TITLE: Som iguals?
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Introdueix un caràcter en minúscula: ")
    var charLower = scanner.next().single()
    print("Introdueix un caràcter en majúscula: ")
    var charUpper = scanner.next().single()

    println(charLower.lowercaseChar() == charUpper.lowercaseChar())
}