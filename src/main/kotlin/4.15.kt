import java.util.Scanner

/*
* AUTHOR: Erik Vasilyan
* DATE: 21/10/2022
* TITLE: La suma total
*/

fun main(args: Array<String>) {

    var result = 0
    var correct = true

    for(i in args.indices) {
        result += args[i].toInt()
    }

    for(arg in args) {
        if (arg.toInt() == result - arg.toInt()) {
            correct = true
            break
        }
        else {
            correct = false
        }
    }
    if (correct) println("si")
    else println("no")
}