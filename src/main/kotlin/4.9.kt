/*
* AUTHOR: Erik Vasilyan
* DATE: 19/10/2022
* TITLE: Valors repetits
*/

fun main(args: Array<String>) {

    for(i in 0 .. args.size) {
        for (j in i+1 until args.size) {
            if (args[i].toInt() == args[j].toInt()) println(args[i])
        }
    }
}