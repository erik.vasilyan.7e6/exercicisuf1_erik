import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 21/10/2022
* TITLE: Substitueix el caràcter
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val userString = scanner.next()
    val firstChar = scanner.next().single()
    val secondChar = scanner.next().single()

    println(userString.replace(firstChar, secondChar))
}