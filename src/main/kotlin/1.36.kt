import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 22/09/2022
* TITLE: Construeix la història
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Introdueix la primera frase: ")
    var primeraFrase = scanner.nextLine()
    print("Introdueix la segona frase: ")
    var segonaFrase = scanner.nextLine()
    print("Introdueix la terçera frase: ")
    var terceraFrase = scanner.nextLine()

    println("$primeraFrase $segonaFrase $terceraFrase")
}