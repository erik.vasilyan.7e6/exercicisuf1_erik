import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 21/10/2022
* TITLE: Hola i adeu
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val userString = scanner.next()
    var correct = true

    for (i in userString.indices) {
        if (userString[i] == 'h' && userString[i+1] == 'o' && userString[i+2] == 'l' && userString[i+3] == 'a') {
            correct = true
            break
        }
        else correct = false
    }
    if (correct) println("hola")
    else println("adeu")
}