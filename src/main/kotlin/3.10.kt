import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 13/10/2022
* TITLE: Extrems
*/

fun main() {

    val scanner = Scanner(System.`in`)

    var min = 999999999
    var max = -999999999

    do {
        val n = scanner.nextInt()

        if(n > max && n != 0) max = n
        if(n < min && n != 0) min = n

    } while (n != 0)
    println("$max $min")
}
