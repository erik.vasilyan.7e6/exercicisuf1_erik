import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 21/09/2022
* TITLE: És una lletra?
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Introdueix un caràcter: ")
    var char = scanner.next().single()

    // println(char.isLetter())

    println(char.code > 64 && char.code < 91 || char.code > 96 && char.code < 123)
}