import java.util.Scanner

/*
* AUTHOR: Erik Vasilyan
* DATE: 17/11/2022
* TITLE: Elimina les repeticions
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val n = scanner.nextInt()

    val sequence = mutableListOf<Int>()
    val finalSequence = mutableListOf<Int>()

    repeat(n) { sequence.add(scanner.nextInt()) }


    for(number in sequence) {
        if (number !in finalSequence) finalSequence.add(number)
    }

    println(finalSequence)
}

