import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 29/09/2022
* TITLE: Parell o senar?
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val number = scanner.nextInt()

    if (number % 2 == 0)  println("parell")
    else println("senar")
}