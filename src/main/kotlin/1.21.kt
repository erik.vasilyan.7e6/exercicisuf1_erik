import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 21/09/2022
* TITLE: Tres nombres iguals
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Introdueix el primer nombre: ")
    var primerNombre = scanner.nextInt()

    print("Introdueix el segon nombre: ")
    var segonNombre = scanner.nextInt()

    print("Introdueix el terçer nombre: ")
    var tercerNombre = scanner.nextInt()

    println(primerNombre == segonNombre && segonNombre == tercerNombre)
}