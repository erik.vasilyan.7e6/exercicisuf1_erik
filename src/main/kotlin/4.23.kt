import java.util.*

/*
* AUTHOR: Erik Vasilyan
* DATE: 21/10/2022
* TITLE: Inverteix les paraules
*/

fun main() {
    val scanner = Scanner(System.`in`)

    val repeat = true

    while (repeat) {
        val userString = scanner.next()

        for (i in userString.indices) {
            print(userString[userString.length-1-i])
        }
        println("")
    }
}